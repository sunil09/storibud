<?php
require_once ('Classes/USERCLASS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$userClass = new \Classes\USERCLASS();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $userClass->apiResponse($response);
    return false;
}
$type = $_POST['type'];
if($type == "register")
{
    $requiredfields = array('username','email','contactNumber','address','password','registersource','plan','renewal_type',
    'auto_renewal');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $imageResponse = $userClass->link->storeImage('userImage','images');
    $file_name = $imageResponse['File_Name'];
    $username = trim($_POST['username']);
    $email = trim($_POST['email']);
    $password = (MD5(trim($_POST['password'])));
    $contactNumber = trim($_POST['contactNumber']);
    $address = trim($_POST['address']);
    $registerSource = trim($_POST['registersource']);
    $plan = trim($_POST['plan']);
    $renewal_type = trim($_POST['renewal_type']);
    $auto_renewal = trim($_POST['auto_renewal']);
    ($response = $userClass->checkEmailExistence($email));
    if($response[STATUS] == Success) {
        $token = $userClass->generateToken();
        $response = $userClass->registerUser($username, $email, $contactNumber, $address, $password,$registerSource,$plan,
        $renewal_type,$auto_renewal,$token,$file_name);
    }
    if($response[STATUS] == Error){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $response['userId'];
    $temp = $userClass->getParticularUserData($userId);
    $response['userData'] = $temp['UserData'];
    $response['UserImageBaseURL'] = ImagesBaseURL;
    unset($response['userId']);
    $userClass->apiResponse($response);
}
else if($type == "signIn"){
    $requiredfields = array('email','password');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $email = trim($_POST['email']);
    $password = (MD5(trim($_POST['password'])));
    $response = $userClass->signIn($email,$password);
    if($response[STATUS] == Error){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $response['userId'];
    $temp = $userClass->getParticularUserData($userId);
    $response['userData'] = $temp['UserData'];
    $response['UserImageBaseURL'] = ImagesBaseURL;
    unset($response['userId']);
    $userClass->apiResponse($response);
}
else if($type == "getParticularUserData")
{
    $requiredfields = array('userId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $response = $userClass->getParticularUserData($userId);
    $userClass->apiResponse($response);
}
else if($type == "statusChange")
{
    $requiredfields = array('user_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $user_id = $_REQUEST['user_id'];
    $response = $userClass->statusChange($user_id,$value);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "renewalStatusChange")
{
    $requiredfields = array('user_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $user_id = $_REQUEST['user_id'];
    $response = $userClass->renewalStatusChange($user_id,$value);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "deleteUser")
{
    $requiredfields = array('user_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $user_id = $_REQUEST['user_id'];
    $response = $userClass->deleteUser($user_id);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
///////////////////////////////////////////////////*/
/*else if($type == "updateUserInfo")
{
    $requiredfields = array('userId','token','userName','email','contactNumber','address');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $token = $_POST['token'];
    $tokenResponse = $userClass->link->isValidToken($userId,$token);
    if($tokenResponse[STATUS] == Error)
    {
        $userClass->apiResponse($tokenResponse);
        return false;
    }
    $userName = $_POST['userName'];
    $contactNumber = $_POST['contactNumber'];
    $email = $_POST['email'];
    $address = $_POST['address'];
    $token = $userClass->generateToken();
    $response = $userClass->updateUser($userId, $userName, $email, $contactNumber, $token, $address);
    if($response['Status'] == 'Failure')
    {
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $response['userId'];
    $temp = $userClass->getParticularUserData($userId);
    $response['UserData'] = $temp['UserData'];
    $response['ImagesBaseURL'] = ImagesBaseURL;
    unset($response['userId']);
    $userClass->apiResponse($response);
}
/*else if($type == "addContactNumber")
{
    $requiredfields = array('userId','token','contactNumber','countryCode');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $token = $_POST['token'];
    $tokenResponse = $userClass->link->isValidToken($userId,$token);
    if($tokenResponse[STATUS] == Error)
    {
        $userClass->apiResponse($tokenResponse);
        return false;
    }
    $contactNumber = $_POST['contactNumber'];
    $countryCode = $_POST['countryCode'];
    $response = $userClass->addContactNumber($userId, $contactNumber, $countryCode);
    $userClass->apiResponse($response);
}
else if($type == "addLocation")
{
    $requiredfields = array('userId','token','lat','lng','address');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $token = $_POST['token'];
    $tokenResponse = $userClass->link->isValidToken($userId,$token);
    if($tokenResponse[STATUS] == Error)
    {
        $userClass->apiResponse($tokenResponse);
        return false;
    }
    $lat = $_POST['lat'];
    $lng = $_POST['lng'];
    $address = $_POST['address'];
    $response = $userClass->addLocation($userId, $lat, $lng, $address,'idleState','','','','','','','','','','');
    $userClass->apiResponse($response);
}
if($type == "getContactList") /////done/////
{
    $contactProfile = "";
    $NotRegistered = array();
    $Registered = array();
    $isRegistered = 'No';
    $requiredfields = array('contactJson');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $contactJson = $_POST['contactJson'];
    $contactJson = json_decode($contactJson,true);
    $userId = $contactJson['userId'];
    $token = $contactJson['token'];
    $tokenResponse = $userClass->link->isValidToken($userId,$token);
    if($tokenResponse[STATUS] == Error)
    {
        $userClass->apiResponse($tokenResponse);
        return false;
    }
    $contactList = $contactJson['contactList'];
    for($i=0;$i<sizeOf($contactList);$i++)
    {
        $contactName = $contactList[$i]['name'];
        $contactNumber = $contactList[$i]['number'];
        ($response = $userClass->isContactRegistered($contactNumber));
        if($response[STATUS] == Success)
        {
            if($response[MESSAGE] == false){
                //store to not registered Array
                $isRegistered = 'No';
                $NotRegistered[] = array("contactName"=>$contactName,"contactNumber"=>$contactNumber);
            }
            else {
                //store to registered Array
                $isRegistered = 'yes';
                $contactName = $response['userName'];
                $contactProfile = $response['userProfile'];
                $userId = $response['userId'];
                $userEmail = $response['userEmail'];
                $userAddress = $userClass->getAddressOfUser($userId);
                if($userAddress[STATUS] != Error)
                {
                    $userAddress = $userAddress['Address'];
                    $Registered[] = array("contactName"=>$contactName,"contactNumber"=>$contactNumber,
                        "contactProfile"=>$contactProfile,"contactEmail"=>$userEmail,"contactId"=>$userId,
                        "userAddress"=>$userAddress);
                }
            }
        }
        $response = $userClass->insertContact($contactName,$contactNumber,$contactProfile,$isRegistered);
        if($response[STATUS] == Error)
        {
            continue;
        }
    }
    unset($response['userName']);
    unset($response['userProfile']);
    unset($response['userId']);
    unset($response['userEmail']);
    unset($response['Address']);
    $response['NotRegistered'] = $NotRegistered;
    $response['Registered'] = $Registered;
    $userClass->apiResponse($response);
}
else if($type == "onCallAction")
{
    $isRegistered = "No";
    $requiredfields = array('userId','token','lat','lng','address','contactNumber','countryCode','locationType','callTime');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $token = $_POST['token'];
    $userId = $_POST['userId'];
    $tokenResponse = $userClass->link->isValidToken($userId,$token);
    if($tokenResponse[STATUS] == Error)
    {
        $userClass->apiResponse($tokenResponse);
        return false;
    }
    $contactNumber = $_POST['contactNumber'];
    $countryCode = $_POST['countryCode'];
    $locationType = $_POST['locationType'];
    $callTime = $_POST['callTime'];
    $my_lat = $_POST['lat'];
    $my_lng = $_POST['lng'];
    $my_address = $_POST['address'];
    $contactResponse = $userClass->isContactRegistered($contactNumber);
    if($contactResponse[STATUS] == Error)
    {
        $userClass->apiResponse($contactResponse);
        return false;
    }
    if($contactResponse[MESSAGE])
    {
        $isRegistered = "Yes";
        $contactName = $contactResponse['userName'];
        $contactEmail = $contactResponse['userEmail'];
        $contactId = $contactResponse['userId'];
        $contactProfile = $contactResponse['userProfile'];
        $contactAddressResponse = $userClass->getAddressOfUser($contactId);
        if(sizeOf($contactAddressResponse['Address'])>0){
            $contactLatitude = $contactAddressResponse['Address'][0]['latitude'];
            $contactLongtitude = $contactAddressResponse['Address'][0]['longtitude'];
            $contactAddress = $contactAddressResponse['Address'][0]['address'];
            $contactDated = $contactAddressResponse['Address'][0]['dated'];
            $contactDateStamp = $contactAddressResponse['Address'][0]['datestamp'];
            $contactTimeStamp = $contactAddressResponse['Address'][0]['timestamp'];
        }
        else{
            $contactLatitude = "";
            $contactLongtitude = "";
            $contactAddress = "";
            $contactDated = "";
            $contactDateStamp = "";
            $contactTimeStamp="";
        }
        $response = $userClass->addLocation($userId, $my_lat, $my_lng, $my_address,$locationType,$callTime,
            $contactNumber,$contactName,$isRegistered,$contactProfile,$contactEmail,$contactId,$contactLatitude,
            $contactLongtitude,$contactAddress);
        if($response[STATUS] == Success) {
            $response["UserData"] = array("userName"=>$contactName,"userEmail"=>$contactEmail,"userProfile"=>$contactProfile
            ,"userId"=>$contactId,"userLatitude"=>$contactLatitude,"userLongtitude"=>$contactLongtitude,
                "userAddress"=>$contactAddress,"userContact"=>$contactNumber,"isRegistered"=>$isRegistered,"contactDated"=>$contactDated,
                "contactDateStamp"=>$contactDateStamp,"contactTimeStamp"=>$contactTimeStamp);
        }
    }
    else{
        $isRegistered = "No";
        $response[STATUS] = Success;
        $response[MESSAGE] = "Not Registered !!! Invite this User";
        $temp = $userClass->isNumberExistInContactList($contactNumber,$countryCode);
        if($temp[STATUS] == Error){
            $userClass->apiResponse($temp);
            return false;
        }
        $contactName = $temp['userName'];
        $isRegistered = $temp['isRegistered'];
        $response = $userClass->addLocation($userId, $my_lat, $my_lng, $my_address,$locationType,$callTime,
            $contactNumber,$contactName,$isRegistered,"","","","","","");
        if($response[STATUS] == Success) {
            $response["UserData"] = array("userName"=>$contactName,"userContact"=>$contactNumber,
                "isRegistered"=>$isRegistered);
        }
    }
    unset($response['userName']);
    unset($response['userProfile']);
    unset($response['userId']);
    unset($response['userEmail']);
    unset($response['Address']);
    unset($response['isRegistered']);
    $userClass->apiResponse($response);
}
else if($type == "getLocationsOfUser")
{
    $requiredfields = array('userId','days','token');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $token = $_POST['token'];
    $days = (int)$_POST['days'];
    if($days>0 && $days<=10) {
        $tokenResponse = $userClass->link->isValidToken($userId, $token);
        if ($tokenResponse[STATUS] == Error) {
            $userClass->apiResponse($tokenResponse);
            return false;
        }
        $response = $userClass->getAllAddressesOfUser($userId, $days);
    }
    else{
        $response[STATUS] = Error;
        $response[MESSAGE] = "Numbers of Days Should Not be Less than 1 and Should Not be Greater than 10";
    }
    $userClass->apiResponse($response);
}
else if($type == "SearchDetail"){
    $requiredfields = array('userId','token','contactNumber','countryCode');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $token = $_POST['token'];
    $tokenResponse = $userClass->link->isValidToken($userId,$token);
    if($tokenResponse[STATUS] == Error)
    {
        $userClass->apiResponse($tokenResponse);
        return false;
    }
    $contactNumber = $_POST['contactNumber'];
    $countryCode = $_POST['countryCode'];
    $regservice = $userClass->isContactRegistered($contactNumber);
    if($regservice[STATUS] != Success){
        $userClass->apiResponse($regservice);
        return false;
    }
    unset($response['userName']);
    unset($response['userProfile']);
    unset($response['userId']);
    unset($response['userEmail']);
    unset($response['Address']);
    unset($response['isRegistered']);
    if($regservice[MESSAGE] == false){
        $response[STATUS] = Error;
        $response[MESSAGE] = "User Is Not Registered With Us";
        $ttemp = $userClass->isNumberExistInContactList($contactNumber,$countryCode);
        $response['userName'] = $ttemp['name'];
        $response['isRegistered'] = $ttemp['registered'];
    }
    else{
        $response[STATUS] = Success;
        $response[MESSAGE] = "User Data Found";
        $response['userName'] = $regservice['userName'];
        $response['userProfile'] = $regservice['userProfile'];
        $response['userId'] = $regservice['userId'];
        $response['isRegistered'] = "Yes";
        $response['userEmail'] = $regservice['userEmail'];
        $address = $userClass->getAddressOfUser($regservice['userId']);
        if($address[STATUS] != Success){
            $userClass->apiResponse($address);
            return false;
        }
        $response['Address'] = $address = $address['Address'];
    }
    $userClass->apiResponse($response);
}
else if($type == "SearchDetailWeb"){
    $requiredfields = array('contactNumber','countryCode');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $contactNumber = $_POST['contactNumber'];
    $countryCode = $_POST['countryCode'];
    $regservice = $userClass->isContactRegistered($contactNumber);
    if($regservice[STATUS] != Success){
        $userClass->apiResponse($regservice);
        return false;
    }
    unset($response['userName']);
    unset($response['userProfile']);
    unset($response['userId']);
    unset($response['userEmail']);
    unset($response['Address']);
    unset($response['isRegistered']);
    if($regservice[MESSAGE] == false){
        $response[STATUS] = Error;
        $response[MESSAGE] = "User Is Not Registered With Us";
        ($ttemp = $userClass->isNumberExistInContactList($contactNumber,$countryCode));
        $response['userName'] = $ttemp['userName'];
        $response['isRegistered'] = $ttemp['isRegistered'];
    }
    else{
        $response[STATUS] = Success;
        $response[MESSAGE] = "User Data Found";
        $response['userName'] = $regservice['userName'];
        $response['userProfile'] = $regservice['userProfile'];
        $response['userId'] = $regservice['userId'];
        $response['userEmail'] = $regservice['userEmail'];
        $response['isRegistered'] = "Yes";
        $address = $userClass->getAddressOfUser($regservice['userId']);
        if($address[STATUS] != Success){
            $userClass->apiResponse($address);
            return false;
        }
        $response['Address'] = $address['Address'];
    }
    $userClass->apiResponse($response);
}*/
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $userClass->apiResponse($response);
}
?>