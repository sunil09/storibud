<?php
require_once ('Classes/CATEGORY.php');
require_once ('Classes/BOOKS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$catClass = new \Classes\CATEGORY();
$booksClass = new \Classes\BOOKS();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $catClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "addCategory")
{
    $requiredfields = array('cat_name','cat_status');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $cat_name = trim($_POST['cat_name']);
    $cat_status = trim($_POST['cat_status']);
    ($response = $catClass->checkCategoryExistence($cat_name));
    if($response[STATUS] == Success) {
        $response = $catClass->addCategory($cat_name,$cat_status);
    }
    if($response[STATUS] == Error){
        $catClass->apiResponse($response);
        return false;
    }
    $catId = $response['catId'];
    $temp = $catClass->getParticularCatData($catId);
    $response['catData'] = $temp['catData'];
    $response[ImagesBaseURLKey] = ImagesBaseURL;
    unset($response['catId']);
    $catClass->apiResponse($response);
}
else if($type == "editCategory")
{
    $requiredfields = array('cat_name','cat_status','cat_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $cat_name = trim($_POST['cat_name']);
    $cat_status = trim($_POST['cat_status']);
    $cat_id = trim($_POST['cat_id']);
    $imageChanged = trim($_POST['imageChanged']);
    $response = $catClass->editCategory($cat_name,$cat_status,$cat_id,$imageChanged);
    if($response[STATUS] == Error){
        $catClass->apiResponse($response);
        return false;
    }
    $catId = $response['catId'];
    $temp = $catClass->getParticularCatData($catId);
    $response['catData'] = $temp['catData'];
    $response[ImagesBaseURLKey] = ImagesBaseURL;
    unset($response['catId']);
    $catClass->apiResponse($response);
}
else if($type == "getCategories")
{
    $response = $catClass->getAllCategories();
    $catClass->apiResponse($response);
}
else if($type == "getCategory"){
    $requiredfields = array('cat_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $cat_id = $_REQUEST['cat_id'];
    $response = $catClass->getParticularCatData($cat_id);
    $catClass->apiResponse($response);
}
else if($type == "statusChange")
{
    $requiredfields = array('cat_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $cat_id = $_REQUEST['cat_id'];
    $response = $catClass->statusChange($cat_id,$value);
    if($response[STATUS] == Error) {
        $catClass->apiResponse($response);
        return false;
    }
    $catClass->apiResponse($response);
}
else if($type == "deleteCategory")
{
    $requiredfields = array('cat_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $cat_id = $_REQUEST['cat_id'];
    $response = $catClass->deleteCategory($cat_id);
    if($response[STATUS] == Error) {
        $catClass->apiResponse($response);
        return false;
    }
    $catClass->apiResponse($response);
}
else if($type='allBooksData'){
    $requiredfields = array('cat_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $cat_id = $_REQUEST['cat_id'];
    $response = $catClass->getBookCount($cat_id);
    if($response[STATUS] == Error) {
        $catClass->apiResponse($response);
        return false;
    }
    $catClass->apiResponse($response);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $catClass->apiResponse($response);
}
?>