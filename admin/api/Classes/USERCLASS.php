<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 1/5/17
 * Time: 1:27 PM
 */
namespace Classes;
require_once('CONNECT.php');
class USERCLASS
{
    public $link = null;
    public $response = array();
    public $currentDate=null;
    public $currentDateStamp=null;
    public $currentDateTime=null;
    public $currentDateTimeStamp=null;
    function __construct()
    {
        $this->link = new CONNECT();
        $this->currentDate = date('d M Y');
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateStamp = strtotime($this->currentDate);
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function registerUser($username,$email,$contactNumber,$address,$password,$registersource,$plan,$renewal_type,
    $auto_renewal,$token,$file_name)
    {
        $link = $this->link->connect();
        if($link) {
            if($renewal_type == "Monthly"){
                $days = $this->link->currentMonthDays();
                $expiry_date = ($this->currentDateTimeStamp+(86400*$days))-86400;
            }
            elseif ($renewal_type == "Yearly"){
                $days = $this->link->YearDays();
                $expiry_date = ($this->currentDateTimeStamp+(86400*$days))-86400;
            }
            $query = "insert into users (user_name,user_email,password,user_contact,active_plan,activation_date,
            plan_expiry_date,user_status,user_token,user_address,register_source,renewal_type,auto_renewal,email_verified,user_profile)
            VALUES ('$username','$email','$password','$contactNumber','$plan','$this->currentDateTimeStamp','$expiry_date',
            '1','$token','$address','$registersource','$renewal_type','$auto_renewal','no','$file_name')";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Verification Link has been Sent to your Registered E-Mail Address ";
                $this->response['userId'] = $this->link->getLastId();
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function signIn($email, $password)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_email = '$email' and password = '$password'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $rows = mysqli_fetch_array($result);
                    $user_status = $rows['user_status'];
                    if($user_status == "1") {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Login Success";
                        $this->response['userId'] = $rows['user_id'];
                    }
                    else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = "Sorry !!! Your Account has been Suspended Please Contact Administrator";
                    }
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Login Credentials";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function generateToken()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $charactersLength; $i++) {
            $randomString .= $characters[rand(0,15)];
        }
        return $randomString;
    }
    public function checkEmailExistence($email)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from users where user_email = '$email'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "E-Mail Address Already Registered";
                    $row = mysqli_fetch_array($result);
                    $this->response['userId'] = $row['user_id'];
                }
                else{
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Now Register";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularUserData($userId)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from users where user_id='$userId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $userData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid User";
                    $this->response['UserData'] = $userData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid User";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function updateUser($userId,$username,$email,$contactNumber,$token,$address)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "update users set user_name='$username',user_email='$email',
            user_contact='$contactNumber',user_token = '$token',user_address = '$address' where user_id='$userId' ";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Success";
                $this->response['userId'] = $userId;
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    ///////////////////////////////////////////////////////
    public function getUserDataFromEmail($email)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from users where user_email='$email'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $userData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid User";
                    $this->response["UserImageBaseURL"] = UserImageBaseURL;
                    $this->response['UserData'] = $userData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid User";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function statusChange($user_id,$value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE users SET user_status='$value' WHERE user_id='$user_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function renewalStatusChange($user_id,$value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE users SET auto_renewal='$value' WHERE user_id='$user_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function deleteUser($user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from users WHERE user_id='$user_id'");
                    if ($update) {
                        $row = mysqli_fetch_array($result);
                        if($row['user_profile'] != "") {
                            unlink("Files/images/" . $row['user_profile']);
                        }
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Users Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getAllUsers(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_array($result)){
                        $usersData[]=array(
                          "user_id"=>$rows["user_id"],
                          "user_name"=>$rows["user_name"],
                          "user_email"=>$rows["user_email"],
                          "user_contact"=>$rows["user_contact"],
                          "active_plan"=>$rows["active_plan"],
                          "activation_date"=>$rows["activation_date"],
                          "plan_expiry_date"=>$rows["plan_expiry_date"],
                          "user_status"=>$rows["user_status"],
                          "user_address"=>$rows["user_address"],
                          "renewal_type"=>$rows["renewal_type"],
                          "auto_renewal"=>$rows["auto_renewal"],
                          "email_verified"=>$rows["email_verified"],
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['usersData'] = $usersData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}