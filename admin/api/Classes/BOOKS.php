<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/21/2017
 * Time: 5:36 PM
 */

namespace Classes;
require_once('CONNECT.php');
require_once('USERCLASS.php');
require_once('CATEGORY.php');
class BOOKS
{
    public $userClass = null;
    public $category = null;
    public $link = null;
    public $response = array();

    function __construct()
    {
        $this->link = new CONNECT();
        $this->userClass = new USERCLASS();
        $this->category = new CATEGORY();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function addBook($cat_id,$book_title,$book_desc,$book_author,$book_narrator,$book_price,$book_status,$discount_on_book)
    {
        $link = $this->link->connect();
        if ($link) {
            $audio_response = $this->link->storeImage('book_audio','audio');
            if ($audio_response[STATUS] == Error) {

                $this->response[STATUS] = $audio_response[STATUS];
                $this->response[MESSAGE] = $audio_response[MESSAGE];
                return $this->response;
            }
            $audio_file = $audio_response['File_Name'];
            $short_audio_response = $this->link->storeImage('book_short_audio','audio');
            if ($short_audio_response[STATUS] == Error) {

                $this->response[STATUS] = $short_audio_response[STATUS];
                $this->response[MESSAGE] = $short_audio_response[MESSAGE];
                return $this->response;
            }
            $short_audio_file = $short_audio_response['File_Name'];
            $image_response = $this->link->storeImage('book_image','images');
            if ($image_response[STATUS] == Error) {
                $this->response[STATUS] = $image_response[STATUS];
                $this->response[MESSAGE] = $image_response[MESSAGE];
                return $this->response;
            }
            $image_file = $image_response['File_Name'];
            $mp3file = new \MP3File("Files/audio/".$audio_file);
            $duration2 = $mp3file->getDuration();//(slower) for VBR (or CBR)
            $finalDuration = \MP3File::formatTime($duration2);
            $query = "insert into books (cat_id,book_name,book_desc,book_author,book_narrator,play_time,
            list_price,discount_id,book_status,audio_file,short_audio_file,front_look) VALUES ('$cat_id','$book_title','$book_desc',
            '$book_author','$book_narrator','$finalDuration','$book_price','$discount_on_book','$book_status',
            '$audio_file','$short_audio_file','$image_file')";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "New Book Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function editBook($book_id,$book_title,$book_desc,$book_author,$book_narrator,$book_price,$cat_id,$book_status,
    $imageChanged,$audioChanged,$shortaudioChanged,$discount_on_book)
    {
        $link = $this->link->connect();
        if ($link) {
            $book_audio="";
            $image_name="";
            if($imageChanged == "yes") {
                $image_response = $this->link->storeImage('book_image', 'images');
                if ($image_response[STATUS] == Error) {
                    $this->response[STATUS] = $image_response[STATUS];
                    $this->response[MESSAGE] = $image_response[MESSAGE];
                    return $this->response;
                }
                $image_name = $image_response['File_Name'];
            }
            if($audioChanged == "yes") {
                $audio_response = $this->link->storeImage('book_audio', 'audio');
                if ($audio_response[STATUS] == Error) {
                    $this->response[STATUS] = $audio_response[STATUS];
                    $this->response[MESSAGE] = $audio_response[MESSAGE];
                    return $this->response;
                }
                $book_audio = $audio_response['File_Name'];
                $mp3file = new \MP3File("Files/audio/".$book_audio);
                $duration2 = $mp3file->getDuration();//(slower) for VBR (or CBR)
                $finalDuration = \MP3File::formatTime($duration2);
            }
            if($shortaudioChanged == "yes") {
                $short_audio_response = $this->link->storeImage('book_short_audio', 'audio');
                if ($short_audio_response[STATUS] == Error) {
                    $this->response[STATUS] = $short_audio_response[STATUS];
                    $this->response[MESSAGE] = $short_audio_response[MESSAGE];
                    return $this->response;
                }
                $book_short_audio = $short_audio_response['File_Name'];
            }
            if($imageChanged == "yes" && $audioChanged == "yes" && $shortaudioChanged == "yes"){
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',
                book_author='$book_author', book_narrator='$book_narrator',play_time='$finalDuration',
                list_price='$book_price',discount_id='$discount_on_book', book_status='$book_status',
                audio_file='$book_audio',short_audio_file='$book_short_audio',front_look='$image_name' 
                where book_id = '$book_id'";
            }
            else if($imageChanged == "yes" && $audioChanged == "no" && $shortaudioChanged == "no"){
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',
                book_author='$book_author', book_narrator='$book_narrator',play_time='$finalDuration',
                list_price='$book_price',discount_id='$discount_on_book', book_status='$book_status',
                front_look='$image_name' where book_id = '$book_id'";
            }
            else if($imageChanged == "yes" && $audioChanged == "yes" && $shortaudioChanged == "no"){
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',
                book_author='$book_author', book_narrator='$book_narrator',play_time='$finalDuration',
                list_price='$book_price',discount_id='$discount_on_book', book_status='$book_status',
                audio_file='$book_audio',front_look='$image_name' where book_id = '$book_id'";
            }
            else if($imageChanged == "yes" && $audioChanged == "no" && $shortaudioChanged == "yes"){
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',
                book_author='$book_author', book_narrator='$book_narrator',play_time='$finalDuration',
                list_price='$book_price',discount_id='$discount_on_book', book_status='$book_status',
                short_audio_file='$book_short_audio',front_look='$image_name' where book_id = '$book_id'";
            }
            else if($imageChanged == "no" && $audioChanged == "yes" && $shortaudioChanged == "no"){
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',
                book_author='$book_author', book_narrator='$book_narrator',play_time='$finalDuration',
                list_price='$book_price',discount_id='$discount_on_book', book_status='$book_status',
                audio_file='$book_audio' where book_id = '$book_id'";
            }
            else if($imageChanged == "no" && $audioChanged == "yes" && $shortaudioChanged == "yes"){
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',
                book_author='$book_author', book_narrator='$book_narrator',play_time='$finalDuration',
                list_price='$book_price',discount_id='$discount_on_book', book_status='$book_status',
                audio_file='$book_audio',short_audio_file='$book_short_audio' where book_id = '$book_id'";
            }
            else if($imageChanged == "no" && $audioChanged == "no" && $shortaudioChanged == "yes"){
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',
                book_author='$book_author', book_narrator='$book_narrator',play_time='$finalDuration',
                list_price='$book_price',discount_id='$discount_on_book', book_status='$book_status',
                short_audio_file='$book_short_audio' where book_id = '$book_id'";
            }
            else {
                $query = "update books set cat_id='$cat_id',book_name='$book_title',book_desc='$book_desc',book_author='$book_author',
                book_narrator='$book_narrator',list_price='$book_price',discount_id='$discount_on_book',
                book_status='$book_status' where book_id = '$book_id'";
            }
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Book Detail Updated SuccessFully";
                $this->response['bookId'] = $book_id;
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function checkBookExistence($book_title, $cat_id)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "select * from books where cat_id = '$cat_id' and book_name = '$book_title'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Book Already Exist Please Enter Diffrent Name";
                    $row = mysqli_fetch_array($result);
                    $this->response['bookId'] = $row['book_id'];
                } else {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid Name";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularBookData($bookId)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from books where book_id='$bookId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $bookData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Book Exist";
                    $this->response['bookData'] = $bookData;
                    $cat_id = $bookData['cat_id'];
                    $cat_data = $this->category->getParticularCatData($cat_id);
                    $cat_data = $cat_data['catData'];
                    $this->response['Category'] = $cat_data['cat_name'];
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Book Identification";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getAllBooks()
    {
        $catArray = array();
        $link = $this->link->connect();
        if($link) {
            $query="select * from books order by book_id DESC";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($bookData = mysqli_fetch_array($result)) {
                        $bookArray[]=array(
                            "book_id"=>$bookData['book_id'],
                            "cat_id"=>$bookData['cat_id'],
                            "book_name"=>$bookData['book_name'],
                            "book_desc"=>$bookData['book_desc'],
                            "book_author"=>$bookData['book_author'],
                            "book_narrator"=>$bookData['book_narrator'],
                            "play_time"=>$bookData['play_time'],
                            "list_price"=>$bookData['list_price'],
                            "discount_id"=>$bookData['discount_id'],
                            "book_status"=>$bookData['book_status']
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['data'] = $bookArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Books Found";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getCategoryAllBooks($cat_id)
    {
        $catArray = array();
        $link = $this->link->connect();
        if($link) {
            $query="select * from books where cat_id = '$cat_id' order by book_id DESC";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($bookData = mysqli_fetch_array($result)) {
                        $bookArray[]=array(
                            "book_id"=>$bookData['book_id'],
                            "cat_id"=>$bookData['cat_id'],
                            "book_name"=>$bookData['book_name'],
                            "book_desc"=>$bookData['book_desc'],
                            "book_author"=>$bookData['book_author'],
                            "book_narrator"=>$bookData['book_narrator'],
                            "play_time"=>$bookData['play_time'],
                            "list_price"=>$bookData['list_price'],
                            "discount_id"=>$bookData['discount_id'],
                            "book_status"=>$bookData['book_status']
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['data'] = $bookArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Books Found";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getBookCount($cat_id)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from books where cat_id = '$cat_id'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Count Exist";
                $this->response["Count"] = $num;
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
                $this->response["Count"] = 0;
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            $this->response["Count"] = 0;
        }
        return $this->response;
    }
    public function statusChange($book_id,$value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from books where book_id='$book_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE books SET book_status='$value' WHERE book_id='$book_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function deleteBook($book_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from books where book_id='$book_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from books WHERE book_id='$book_id'");
                    if ($update) {
                        $row = mysqli_fetch_array($result);
                        unlink("Files/images/".$row['front_look']);
                        unlink("Files/audio/".$row['audio_file']);
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Book Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getRatesAndReviews($bookId)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from book_rating where book_id='$bookId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $userRate = 0.0;
                    while($rows = mysqli_fetch_array($result)){
                        $userRate = ((float)$userRate+(float)$rows['user_rate']);
                        $user_id = $rows['user_id'];
                        $user_data = $this->userClass->getParticularUserData($user_id);
                        $user_data =$user_data['UserData'];
                        $rateArray[] =array(
                            "rating_id"=>$rows['rating_id'],
                            "user_rate"=>$rows['user_rate'],
                            "user_review"=>$rows['user_review'],
                            "user_name"=>$user_data['user_name'],
                            "user_image"=>$user_data['user_profile']
                        );
                    }
                    $userRate = $userRate/$num;
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Book Exist";
                    $this->response['userRate'] = $userRate;
                    $this->response['bookData'] = $rateArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Book Identification";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    function totalBooks(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from books";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
                $this->response['Count'] = $num;
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}