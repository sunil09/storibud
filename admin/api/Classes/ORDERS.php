<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/22/2017
 * Time: 9:18 PM
 */

namespace Classes;
require_once('CONNECT.php');
require_once('BOOKS.php');
require_once('USERCLASS.php');
class ORDERS
{
    public $link = null;
    public $bookClass = null;
    public $userClass = null;
    public $response = array();
    function __construct()
    {
        $this->link = new CONNECT();
        $this->bookClass = new BOOKS();
        $this->userClass = new USERCLASS();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    /*public function addCoupon($coupon_code, $coupon_value, $expiry, $coupon_status)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "insert into discount_coupon (coupon_code,coupon_value,generated_on,expired_on,coupon_status) 
            VALUES ('$coupon_code','$coupon_value','$this->currentDateTimeStamp','$expiry','$coupon_status')";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "New Coupon Code Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function editCoupon($coupon_id,$coupon_code,$coupon_value,$expired_on,$coupon_status)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "update discount_coupon set coupon_code='$coupon_code',coupon_value='$coupon_value',expired_on='$expired_on',
            coupon_status='$coupon_status' where coupon_id = '$coupon_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Coupon Updated SuccessFully";
                $this->response['planId'] = $coupon_id;
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function checkCouponExistence($coupon_code)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "select * from discount_coupon where coupon_code = '$coupon_code'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Coupon Code Already Registered Please Enter Different Code";
                    $row = mysqli_fetch_array($result);
                    $this->response['couponId'] = $row['coupon_id'];
                } else {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid Name";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }*/
    public function getParticularOrderData($order_id)
    {
        $link = $this->link->connect();
        $order_detail=array();
        if($link) {
            $query="select * from orders where order_id='$order_id'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $row = mysqli_fetch_assoc($result);
                    $order_id = $row['order_id'];
                    $query2 = "select * from orderdetail where order_id = '$order_id'";
                    $result2 = mysqli_query($link,$query2);
                    if($result2){
                        $num = mysqli_num_rows($result2);
                        if($num>0){
                            while($detail = mysqli_fetch_array($result2)){
                                $detail_id = $detail['item_id'];
                                $temp = $this->bookClass->getParticularBookData($detail_id);
                                $temp = $temp['bookData'];
                                $order_detail[]=array(
                                    "detail_id"=>$detail['detail_id'],
                                    "book_name"=>$temp['book_name'],
                                    "cat_id"=>$temp['cat_id'],
                                    "book_desc"=>$temp['book_desc'],
                                    "book_author"=>$temp['book_author'],
                                    "book_narrator"=>$temp['book_narrator'],
                                    "play_time"=>$temp['play_time'],
                                    "front_look"=>$temp['front_look'],
                                    "audio_file"=>$temp['audio_file'],
                                    "item_price"=>$detail['item_price']
                                );
                            }
                            $user_id = $row['order_userId'];
                            $userData = $this->userClass->getParticularUserData($user_id);
                            $userData = $userData['UserData'];
                            $order = array(
                                "order_id"=>$row['order_id'],
                                "order_number"=>$row['order_number'],
                                "transaction_id"=>$row['transaction_id'],
                                "amount"=>$row['amount'],
                                "payment_status"=>$row['payment_status'],
                                "order_dated"=>$row['order_dated'],
                                "order_userId"=>$row['order_userId'],
                                "order_user_name" => $userData['user_name'],
                                "order_user_profile" => $userData['user_profile'],
                                ImagesBaseURLKey=>ImagesBaseURL,
                                AudiosBaseURLKey=>AudiosBaseURL,
                                "order_detail"=>$order_detail
                            );
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "Order Data Exist";
                        }
                        else{
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "Empty Order Details";
                        }
                    }
                    else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }

                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Order Found";
                    $this->response['order'] = $order;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Order Identification";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getAllOrders()
    {
        $link = $this->link->connect();
        $order_detail=array();
        $order=array();
        if($link) {
            $query="select * from orders order by order_id desc";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($row = mysqli_fetch_assoc($result)) {
                        $order_id = $row['order_id'];
                        $query2 = "select * from orderdetail where order_id = '$order_id'";
                        $result2 = mysqli_query($link, $query2);
                        if ($result2) {
                            $num = mysqli_num_rows($result2);
                            if ($num > 0) {
                                unset($order_detail);
                                while ($detail = mysqli_fetch_array($result2)) {
                                    $detail_id = $detail['item_id'];
                                    $temp = $this->bookClass->getParticularBookData($detail_id);
                                    $temp = $temp['bookData'];
                                    $order_detail[] = array(
                                        "detail_id" => $detail['detail_id'],
                                        "book_name" => $temp['book_name'],
                                        "book_author" => $temp['book_author'],
                                        "book_narrator" => $temp['book_narrator'],
                                        "play_time" => $temp['play_time'],
                                        "front_look" => $temp['front_look'],
                                        "audio_file" => $temp['audio_file'],
                                        "item_price" => $detail['item_price']
                                    );
                                }
                                $user_id = $row['order_userId'];
                                $userData = $this->userClass->getParticularUserData($user_id);
                                $userData = $userData['UserData'];
                                $order[] = array(
                                    "order_id" => $row['order_id'],
                                    "order_number" => $row['order_number'],
                                    "transaction_id" => $row['transaction_id'],
                                    "amount" => $row['amount'],
                                    "payment_status" => $row['payment_status'],
                                    "order_dated" => $row['order_dated'],
                                    "order_user_id" => $row['order_userId'],
                                    "order_user_name" => $userData['user_name'],
                                    "order_user_profile" => $userData['user_profile'],
                                    ImagesBaseURLKey => ImagesBaseURL,
                                    AudiosBaseURLKey => AudiosBaseURL,
                                    "order_detail" => $order_detail
                                );
                                $this->response[STATUS] = Success;
                                $this->response[MESSAGE] = "Order Data Exist";
                            } else {
                                $this->response[STATUS] = Success;
                                $this->response[MESSAGE] = "Empty Order Details";
                            }
                        } else {
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = $this->link->sqlError();
                        }
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Order Found";
                    $this->response['order'] = $order;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Order Identification";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function refundStatus($order_id, $value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from orders where order_id='$order_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE orders SET refund_generated='$value' WHERE order_id='$order_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    /*public function deleteCoupon($coupon_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from discount_coupon where coupon_id='$coupon_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from discount_coupon WHERE coupon_id='$coupon_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Coupon Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }*/
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
    public function cancelOrder($order_id)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "select * from orders where order_id='$order_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE orders SET order_status='Cancelled' WHERE order_id='$order_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Order Has Been Cancelled Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
}