<?php
require_once('Classes/ORDERS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$orderClass = new \Classes\ORDERS();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $orderClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
/*if($type == "addCoupon")
{
    $requiredfields = array('coupon_code','coupon_value','expiry','coupon_status');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $coupon_code = trim($_POST['coupon_code']);
    $coupon_value = trim($_POST['coupon_value']);
    $expiry = trim($_POST['expiry']);
    $coupon_status = trim($_POST['coupon_status']);
    ($response = $orderClass->checkCouponExistence($coupon_code));
    if($response[STATUS] == Success) {
        $response = $orderClass->addCoupon($coupon_code, $coupon_value, $expiry, $coupon_status);
    }
    if($response[STATUS] == Error){
        $orderClass->apiResponse($response);
        return false;
    }
    $couponId = $response['couponId'];
    $temp = $orderClass->getParticularCouponData($couponId);
    $response['couponData'] = $temp['couponData'];
    unset($response['couponId']);
    $orderClass->apiResponse($response);
}
else if($type == "editCoupon")
{
    $requiredfields = array('coupon_id','coupon_code','coupon_value','expiry','coupon_status');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $coupon_id = trim($_POST['coupon_id']);
    $coupon_code = trim($_POST['coupon_code']);
    $coupon_value = trim($_POST['coupon_value']);
    $expired_on = trim($_POST['expiry']);
    $coupon_status = trim($_POST['coupon_status']);
    $response = $orderClass->editCoupon($coupon_id,$coupon_code,$coupon_value,$expired_on,$coupon_status);
    if($response[STATUS] == Error){
        $orderClass->apiResponse($response);
        return false;
    }
    $couponId = $response['couponId'];
    $temp = $orderClass->getParticularCouponData($couponId);
    $response['couponData'] = $temp['couponData'];
    unset($response['couponId']);
    $orderClass->apiResponse($response);
}*/
if($type == "getOrders")
{
    $response = $orderClass->getAllOrders();
    $orderClass->apiResponse($response);
}
else if($type == "getOrder"){
    $requiredfields = array('order_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $order_id = $_REQUEST['order_id'];
    $response = $orderClass->getParticularOrderData($order_id);
    $orderClass->apiResponse($response);
}
else if($type == "refundStatus")
{
    $requiredfields = array('order_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $order_id = $_REQUEST['order_id'];
    $response = $orderClass->refundStatus($order_id,$value);
    if($response[STATUS] == Error) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else if($type == "cancelOrder")
{
    $requiredfields = array('order_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $order_id = $_REQUEST['order_id'];
    $response = $orderClass->cancelOrder($order_id);
    if($response[STATUS] == Error) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $orderClass->apiResponse($response);
}
?>