<?php
    include('header.php');
    include('api/Classes/CONNECT.php');
    include('api/Constants/DbConfig.php');
    include('api/Constants/configuration.php');
    require_once('api/Classes/CATEGORY.php');
    $conn = new \Classes\CONNECT();
    $category = new \Classes\CATEGORY();
?>
<!-- page content -->
<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row tile_count">
        <a href="users"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
            <div class="count" id="userCount"></div>
            <span class="count_bottom"><i class="green">Click </i>to Expand</span>
        </div></a>
        <a href="books"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-volume-up"></i> Total Audio Books</span>
            <div class="count" id="booksCount"></div>
            <span class="count_bottom"><i class="green"></i> in All Categories</span>
        </div></a>
        <a href="index"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-book"></i> Total Categories</span>
            <div class="count green" id="catCount"></div>
            <span class="count_bottom"><i class="green"></i> Click to Expand</span>
        </div></a>
        <a href="membership"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-users"></i> MemberShip Types</span>
            <div class="count" id="membershipCount"></div>
            <span class="count_bottom"> Click to View</span>
        </div></a>
        <a href="discount"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-cc-discover"></i> Discount Coupons</span>
            <div class="count" id="allCoupon"></div>
            <span class="count_bottom"><i class="green" id="activeCoupon"></i> is Still Active</span>
        </div></a>
        <a href="orders"><div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-first-order"></i> Orders</span>
            <div class="count" id="orderCount"></div>
            <span class="count_bottom"><i class="green"></i>Click to Expand</span>
        </div></a>
    </div>
    <div class="row" role="main">
        <div class="">
            <!--<div class="page-title">
                <div class="title_left"></div>@f1*2C3AmY7+
            </div>-->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>All Categories <small></small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <button onclick="window.location='api/excelProcess.php?dataType=allCats'" class="btn btn-info btn-sm">Download Excel File</button>
                                </li>
                                <li><a class="btn btn-info" onclick="addNewCategory()" style="color: white"><i class="fa fa-plus"></i> Add Category</a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                From here admin can manage/modify the content of the catalogs
                            </p>
                            <table id="catTable" class="table table-striped table-bordered display">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Image</th>
                                        <th>Category</th>
                                        <th>Added On</th>
                                        <th>Books Count</th>
                                        <th>Visibility</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $link = $conn->connect();
                                    if($link) {
                                        $query = "select * from categories order by cat_id DESC";
                                        $result = mysqli_query($link, $query);
                                        if ($result) {
                                            $num = mysqli_num_rows($result);
                                            if ($num > 0) {
                                                $j=0;
                                                while ($catData = mysqli_fetch_array($result)) {
                                                    $j++;
                                                    ?>
                                                    <tr>
                                                        <td data-title='#'><?php echo $j?></td>
                                                        <td data-title='Image'>
                                                            <img src='api/Files/images/<?php echo $catData['cat_image'] ?>' style='height:35px' class='img-thumbnail'>
                                                        </td>
                                                        <td data-title='Category'>
                                                            <a href='cdet?_=<?php echo $catData['cat_id'] ?>'><?php echo $catData['cat_name'] ?></a>
                                                        </td>
                                                        <td data-title='Added On'><?php echo $catData['added_on'] ?></td>
                                                        <?php $countData = $category->getBookCount($catData['cat_id']); ?>
                                                        <td data-title='Books Count'><?php echo $countData['Count'] ?> Books</td>
                                                        <td class='buttonsTd' data-title='Visibility'>
                                                            <?php
                                                            $box="";
                                                            if($catData['cat_status'] == "1") {
                                                                $box = "checked";
                                                            }
                                                            ?>
                                                            <input data-onstyle='info' class='categorystatustoggle' <?php echo $box ?> data-toggle='toggle'
                                                            data-size='mini' data-style='ios' type='checkbox' id="<?php echo $catData['cat_id'] ?>" value="<?php echo $catData['cat_status'] ?>">
                                                        </td>
                                                        <td data-title='Action'>
                                                            <i class='fa fa-edit' onclick=editCategoryData('<?php echo $catData['cat_id'] ?>') style='cursor:pointer;color:#31B0D5'></i>
                                                            &nbsp;&nbsp;
                                                            <i class='fa fa-trash' onclick=deleteCategory('<?php echo $catData['cat_id'] ?>','<?php echo $countData['Count'] ?>') style='color:#D05E61;cursor:pointer'></i>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                        }
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
    include('footer.php');
?>
<script>
    $(document).ready(function() {
        $('#catTable').DataTable( {
        });
    });
</script>
